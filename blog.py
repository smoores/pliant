import json
from datetime import datetime
from flask import Flask, render_template, request, escape
from flask_assets import Environment, Bundle
from jinja2.exceptions import TemplateNotFound

app = Flask(__name__)
assets = Environment(app)

css_bundle = Bundle("styles.css", filters="cssmin", output="gen/%(version)s-styles.css")
assets.register("css", css_bundle)

js_bundle = Bundle("tracking.js", output="gen/%(version)s-scripts.js")
assets.register("js", js_bundle)


def _select_jinja_autoescape(filename):
    """
    Returns `True` if autoescaping should be active for the given template name.
    """
    if filename is None:
        return False
    return filename.endswith(
        (
            ".html",
            ".htm",
            ".xml",
            ".xhtml",
            ".html.jinja",
            ".html.jinja2",
            ".xml.jinja",
            ".xml.jinja2",
            ".xhtml.jinja",
            ".xhtml.jinja2",
        )
    )


app.jinja_env.autoescape = _select_jinja_autoescape


@app.route("/")
def index():
    return render_template("index.html.jinja2")


@app.route("/about_fathom")
def about_fathom():
    return render_template("about_fathom.html.jinja2")


@app.route("/post/<post_slug>")
def post(post_slug=""):
    try:
        return render_template(f"posts/{post_slug}.html.jinja2")
    except TemplateNotFound:
        return render_template("404.html.jinja2"), 404


def extend_post(post):
    new_props = {
        "path": f"{request.url_root}post/{post['slug']}.html",
        "updated": datetime.fromisoformat(post["last_update"]),
        "published": datetime.fromisoformat(post["published"]),
        "content": escape(render_template(f"posts/{post['slug']}.html.jinja2")),
    }
    return {**post, **new_props}


@app.route("/recent.atom")
def recent_feed():
    with open("rss/recent_posts.json", "r") as recent_posts_file:
        posts = json.loads(recent_posts_file.read())
    return render_template(
        "recent.atom.jinja2",
        posts=[extend_post(post) for post in posts],
        url_root=request.url_root,
    )
