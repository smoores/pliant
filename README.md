# Pliant

Pliant is a micro-skeleton for blogging, built on [flask](http://flask.pocoo.org). It's meant for anyone wishing to host their own blog server. As such, it houses the bare bones (a web server, some base templates, and some caching solutions) and leaves everything else up to the user.

## Philosophy

Pliant is a skeleton, not a framework. It attempts to provide sane defaults for a blogging site, in a way that's easy to modify, customize, and use.

## Usage

### Setup

Make sure that you have Python 3.8 available on your system. [Pyenv](https://github.com/pyenv/pyenv) is a good way to manage multiple python versions. Pliant also depends on [poetry](https://python-poetry.org/), see [the install instructions](https://python-poetry.org/docs/#osx-linux-bashonwindows-install-instructions) for more information.

- Clone this repository
- Run `poetry install` to install all of the necessary dependencies

### Running the server

By default, there are production and development server modes. Pliant includes two scripts for running the webserver: `./scripts/start-dev.sh` and `./scripts/start-prd.sh`. The development script will run the flask dev server with `FLASK_ENV=development`, which will restart the server whenever a file is changed.

The production script will run a gunicorn web server with 4 workers. By default, this will be bound to `127.0.0.1:5000`, but the domain and port can be edited in the script.

### Making changes

Pliant does not provide a CMS; all changes are code changes. Adding a blog post is as simple as adding a Jinja2 template to the posts directory, and adding an excerpt to the index. See `templates/posts/example.html.jinja2` for an example. The examples and base CSS try to encourage semantic HTML where appropriate.

Pliant also includes out-of-the-box RSS support. Just add a new JSON object with `"title"`, `"slug"`, `"last_updated"`, and `"published"` to `rss/recent_posts.json` and any RSS subscribers will receive the new post in their feed.

## Analytics

Pliant comes with support for [Fathom](https://usefathom.com). By default, the Fathom info footer and Fathom script are commented out in `templates/base.html.jinja2`. To connect to a self-hosted or paid Fathom server, simply change the URL in `static/tracking.js` and uncomment the footer and script in the base template.
